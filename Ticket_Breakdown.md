# Ticket Breakdown
We are a staffing company whose primary purpose is to book Agents at Shifts posted by Facilities on our platform. We're working on a new feature which will generate reports for our client Facilities containing info on how many hours each Agent worked in a given quarter by summing up every Shift they worked. Currently, this is how the process works:

- Data is saved in the database in the Facilities, Agents, and Shifts tables
- A function `getShiftsByFacility` is called with the Facility's id, returning all Shifts worked that quarter, including some metadata about the Agent assigned to each
- A function `generateReport` is then called with the list of Shifts. It converts them into a PDF which can be submitted by the Facility for compliance.

## You've been asked to work on a ticket. It reads:

**Currently, the id of each Agent on the reports we generate is their internal database id. We'd like to add the ability for Facilities to save their own custom ids for each Agent they work with and use that id when generating reports for them.**


Based on the information given, break this ticket down into 2-5 individual tickets to perform. Provide as much detail for each ticket as you can, including acceptance criteria, time/effort estimates, and implementation details. Feel free to make informed guesses about any unknown details - you can't guess "wrong".


You will be graded on the level of detail in each ticket, the clarity of the execution plan within and between tickets, and the intelligibility of your language. You don't need to be a native English speaker, but please proof-read your work.

## Your Breakdown Here
As a client I want to assign a custom id to Agents I work with and view those custom ids in the generated reports

Scenario: Client opening up a modal to assign custom id
* Given I am on the Agents list page
* When I click "Assign custom identifier" button for "Agent Smith"
* Then I should see a modal with title "Assign custom agent identifier", text input, and a "Save" + "Cancel" button

Scenario: Client assigning custom id
* Given I am on the Agents list page
* And I opened "Assign custom identifier" modal for "Agent smith"
* And I enter value "Matrix protector"
* And I press "Save"
* Then I should see a message "Custom identifier assigned successfully"
* And modal should close

Scenario: Client viewing report
* Given I am on the Agents list page
* And I have "Agent Smith" and "Agent John" assigned
* When I click "Generate report"
* Then I should get a PDF report with 2 rows and 2 columns (id, name)
* And first row should be "Matrix protector | Agent Smith"
* And second row should be "12345 | Agent John"

## Technical tickets
### Backend/API
* Create a new endpoint for clients to allow setting a custom id for each of the Agents assigned to them. Payload should be something like `{ customId: string | null }`
  This value is stored per client (Facility), not directly on the Agent, so make sure to have it saved on the connection between the Agent and Facility. Make the field type
  a varchar(500) or a text, and ensure length is not greater than 500 when validating the input.

* Update `getShiftsByFacility` function to also return new `customId` value for the given facility for each agent.
* Update `generateReport` function to show the `customId` in the PDF report, if the value is set for the specific Agent. If value is null or empty string, fallback to the internal id.

Acceptance criteria:

### Frontend
* In the clients application on the list of assigned agents, add an action or a button that will
  either open up a small modal with the input and a save button, modal title should be "Assign custom agent identifier"
* Allow client to enter any value in the input, with a maximum length of 500
* Send the value to the new endpoint explained above
