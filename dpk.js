const crypto = require("crypto");

// Brief
//
// If function does not have any arguments, TRIVIAL_PARTITION_KEY is always returned.
// There is no need to process rest of the function in that case.
//
// Split the creating of the partition key from the specified event partition key, and the event itself.
// Partition key can vary in type and length, so we need to handle that.
//
// If there's no partition key provided, argument is always coverted to json string and hashed.
// There's also no need to check for the MAX_PARTITION_KEY_LENGTH in this situation because sha3-512
// always returns a string of length 128.

const TRIVIAL_PARTITION_KEY = "0";
const MAX_PARTITION_KEY_LENGTH = 256;

const createHash = (data) =>
  crypto.createHash("sha3-512").update(data).digest("hex");

const createCandidateFromPartitionKey = (data) => {
  let value = data;
  if (typeof value !== "string") {
    value = JSON.stringify(value);
  }

  if (value.length > MAX_PARTITION_KEY_LENGTH) {
    return createHash(value);
  }

  return value;
};

const createCandidateFromEvent = (event) => {
  const data = JSON.stringify(event);
  return createHash(data);
};

exports.deterministicPartitionKey = (event) => {
  if (!event) {
    return TRIVIAL_PARTITION_KEY;
  }

  if (event.partitionKey) {
    return createCandidateFromPartitionKey(event.partitionKey);
  }

  return createCandidateFromEvent(event);
};

exports.createHash = createHash;
