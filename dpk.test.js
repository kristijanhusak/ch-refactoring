const crypto = require("crypto");
const { deterministicPartitionKey, createHash } = require("./dpk");

describe("deterministicPartitionKey", () => {
  it("Returns the literal '0' when given no input", () => {
    const trivialKey = deterministicPartitionKey();
    expect(trivialKey).toBe("0");
  });

  it("Returns the provided partition key as is if it's length is less than 256 (max partition key length)", () => {
    const trivialShortKey = deterministicPartitionKey({
      partitionKey: "test-key",
    });
    expect(trivialShortKey).toEqual("test-key");

    const trivialLongerKey = deterministicPartitionKey({
      partitionKey: "test-longer-key",
    });
    expect(trivialLongerKey).toEqual("test-longer-key");

    // Generate string over max key length
    const overMaxLengthKey = crypto.randomBytes(257).toString("hex");
    const trivialOverMaxLengthKey = deterministicPartitionKey({
      partitionKey: overMaxLengthKey,
    });
    expect(trivialOverMaxLengthKey).not.toEqual(overMaxLengthKey);
  });

  it("Returns sha3-512 hashed partition key if its length is greater than 256 (max partition key length)", () => {
    const overMaxLengthKey = crypto.randomBytes(257).toString("hex");
    const trivialOverMaxLengthKey = deterministicPartitionKey({
      partitionKey: overMaxLengthKey,
    });
    expect(trivialOverMaxLengthKey).toEqual(createHash(overMaxLengthKey));
  });

  it("Converts partition key to json string if non-string is provided", () => {
    const shortObjectKey = deterministicPartitionKey({
      partitionKey: { foo: "bar" },
    });

    expect(shortObjectKey).toEqual(JSON.stringify({ foo: "bar" }));

    const randomObject = {
      _id: "64171240886c2c1064a1684b",
      index: 4,
      guid: "a6e9c15f-50b9-4ebc-a4a9-8eabf56132d0",
      isActive: true,
      balance: "$3,534.75",
      picture: "http://placehold.it/32x32",
      age: 37,
      eyeColor: "brown",
      name: "Dickerson Koch",
      gender: "male",
      company: "ZILLIDIUM",
      email: "dickersonkoch@zillidium.com",
      phone: "+1 (964) 473-3446",
      address: "123 Hausman Street, Cressey, Michigan, 648",
      about:
        "Labore proident deserunt ex anim nulla. Proident irure eu incididunt Lorem sint deserunt occaecat proident. Exercitation irure cillum ex excepteur do. Mollit eu culpa eu proident ullamco consectetur non. Amet mollit nostrud quis officia sint cupidatat.\r\n",
      registered: "2018-11-25T10:38:00 -01:00",
      latitude: 58.50034,
      longitude: -13.972843,
      tags: ["dolore", "ut", "dolor", "irure", "elit", "nulla", "voluptate"],
      friends: [
        {
          id: 0,
          name: "Madeline Bender",
        },
        {
          id: 1,
          name: "Jeannie Decker",
        },
        {
          id: 2,
          name: "Ayers Powell",
        },
      ],
      greeting: "Hello, Dickerson Koch! You have 8 unread messages.",
      favoriteFruit: "apple",
    };
    const result = deterministicPartitionKey({
      partitionKey: randomObject,
    });

    expect(result).toEqual(createHash(JSON.stringify(randomObject)));
  });

  it("Returns strigified and hashed key provided if its not an object with a specific partition key", () => {
    const shortKey = deterministicPartitionKey("test-value");
    expect(shortKey).toEqual(createHash(JSON.stringify("test-value")));

    const overMaxLengthKey = crypto.randomBytes(257).toString("hex");
    const longKey = deterministicPartitionKey(overMaxLengthKey);
    expect(longKey).toEqual(createHash(JSON.stringify(overMaxLengthKey)));
  });
});
